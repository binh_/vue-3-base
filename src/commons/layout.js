import { ref } from "vue";

export default function () {
  let componentKey = ref(0);

  const reRenderComponent = () => {
    componentKey.value += 1;
  };

  const onChangeTitle = (title) => {
    document.title = title;
  };

  return { componentKey, reRenderComponent, onChangeTitle };
}
