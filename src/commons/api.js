const API_PREFIX = "/api/";

export default {
  LOGIN: API_PREFIX + "login",
  REFRESH_TOKEN: API_PREFIX + "refresh-token",
  USER: API_PREFIX + "user",
  LOGOUT: API_PREFIX + "logout",
  GET_TESTS: API_PREFIX + "tests",
  API_QUERY_USER: API_PREFIX + "",
  API_SUSPEND_USER: API_PREFIX + "",
};
