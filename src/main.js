import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "@/store";
import mitt from "mitt";
import i18n from "@/langs/lang";
import {
  ValidationProvider,
  ValidationObserver,
  extend,
  localize,
  setInteractionMode,
} from "vee-validate";
import * as rules from "vee-validate/dist/rules";
// plugins
import VueCookie from "vue-cookies";
import Moment from "moment";

/* Style */
import "@/assets/styles/style.scss";

import Constant from "./commons/constant";
import Api from "./services/api";

i18n.locale = VueCookie.get("locale") || "en";
Moment.locale(i18n.locale);
localize(i18n.locale);

const emitter = mitt();
const app = createApp(App);

app.config.globalProperties.$constant = Constant;
app.config.globalProperties.$api = Api;
app.config.globalProperties.emitter = emitter;

app.use(store).use(router).mount("#app");
