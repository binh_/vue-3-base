import { API_QUERY_USER, API_SUSPEND_USER } from "@/commons/api";
import { ref, reactive, computed } from "vue";

export default {
  setup() {
    const users = reactive([]);
    const usersCurrentPage = ref(1);
    const usersPageSize = this.$constant.PAGE_SIZE;
    const usersCache = reactive([]); // cache user list by format - user_id => user
    const sortUsers = computed(() => {
      if (!Array.isArray(users)) {
        return [];
      }

      return [...users].sort((a, b) => {
        return b.created_at - a.created_at;
        // TODO: Mapping col database
      });
    });
    const usersPageCount = computed(() => {
      return Math.ceil(sortUsers.value.length / usersPageSize);
    });
    const usersTableData = computed(() => {
      return sortUsers.value.slice(
        usersPageSize * (usersCurrentPage.value - 1),
        usersPageSize * usersCurrentPage.value
      );
    });
    const getUsers = async (params, headers, config) => {
      try {
        let response = await this.$api.post(
          API_QUERY_USER,
          params,
          headers,
          config
        );

        if (response.data.code === 200) {
          return response.data.user_info;
        }
      } catch (error) {
        console.log(error);
      }

      return [];
    };

    const deleteUser = async (params, headers, config) => {
      try {
        let response = await this.$api.post(
          API_SUSPEND_USER,
          params,
          headers,
          config
        );
        if (response.data.code === 200) {
          return response;
        }
      } catch (error) {
        console.log(error);
      }

      return false;
    };

    const getUserCache = async (userId) => {
      if (this.usersCache[userId]) {
        return this.usersCache[userId];
      }

      let user = await this.getUsers(
        {
          created_at_ym: "dummy",
          user_id: userId,
          //TODO: Mapping database
        },
        {},
        { showLoading: false }
      );

      if (Array.isArray(user) && user.length) {
        this.usersCache[userId] = user[0];
        return user[0];
      }

      return null;
    };

    const getUserCacheByMail = async (mail) => {
      let user = null;
      for (const key in this.usersCache) {
        const u = this.usersCache[key];
        if (u.mail === mail) {
          user = u;
          break;
        }
      }

      if (user) {
        return user;
      }

      user = await this.getUsers(
        {
          created_at_ym: "dummy",
          mail: mail,
        },
        {},
        { showLoading: false }
      );
      if (Array.isArray(user) && user.length) {
        this.usersCache[user[0].user_id] = user[0];
        return user[0];
      }

      return null;
    };

    return {
      users,
      usersCurrentPage,
      usersPageSize,
      usersCache,
      getUsers,
      deleteUser,
      getUserCache,
      getUserCacheByMail,
      sortUsers,
      usersPageCount,
      usersTableData,
    };
  },
};
